package com.simple.accessdatabase.databaseaccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DatabaseaccessApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseaccessApplication.class, args);
	}

}

