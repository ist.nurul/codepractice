package com.simple.accessdatabase.databaseaccess.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity


public class Kelas {
@Id
    private String namakelas;
    private int jumlahorang;


    public String getNamakelas() {
        return namakelas;
    }

    public void setNamakelas(String namakelas) {
        this.namakelas = namakelas;
    }

    public int getJumlahorang() {
        return jumlahorang;
    }

    public void setJumlahorang(int jumlahorang) {
        this.jumlahorang = jumlahorang;
    }
}
