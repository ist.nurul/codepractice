package com.simple.accessdatabase.databaseaccess.repository;


import com.simple.accessdatabase.databaseaccess.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported =false)
public interface UsersRepository extends JpaRepository<Users, String>{ // simpen file
}
