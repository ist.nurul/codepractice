package com.simple.accessdatabase.databaseaccess.repository;

import com.simple.accessdatabase.databaseaccess.model.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;


@RestResource(exported = false)
@Repository
public interface KelasRepository extends JpaRepository<Kelas, String> {

}
