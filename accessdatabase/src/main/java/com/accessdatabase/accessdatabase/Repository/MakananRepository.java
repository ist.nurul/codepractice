package com.accessdatabase.accessdatabase.Repository;

import com.accessdatabase.accessdatabase.Model.Makanan;

import java.util.List;

public interface MakananRepository {
    public void save(Makanan makanan);
    public void update(Makanan makanan);
    public void delete(Makanan makanan);
    public void getMakanan(Makanan makanan);
    public Makanan getMakanan (String idMakanan);

    public List<Makanan> getMakanan();


}
