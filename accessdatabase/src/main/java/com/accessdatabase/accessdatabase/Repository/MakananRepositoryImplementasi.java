package com.accessdatabase.accessdatabase.Repository;

import com.accessdatabase.accessdatabase.Model.Makanan;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MakananRepositoryImplementasi implements MakananRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Makanan makanan)
    {
        sessionFactory.getCurrentSession().save(makanan);
    }

    @Override
    public void delete(Makanan makanan) {
        sessionFactory.getCurrentSession().delete(makanan);
    }

    @Override
    public void update(Makanan makanan) {
        sessionFactory.getCurrentSession().update(makanan);
    }

    @Override
    public Makanan getMakanan(String idMakanan) {
        return sessionFactory.getCurrentSession().get(Makanan.class, idMakanan);
    }

    @Override
    public List<Makanan> getMakanan() {
        return sessionFactory.getCurrentSession().createCriteria(Makanan.class).list();
    }
}
