package com.accessdatabase.accessdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccessdatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccessdatabaseApplication.class, args);
	}

}

